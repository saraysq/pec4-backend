<?php

class Books extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('books_model');
        $this->load->helper('url_helper');
    }

    public function index()
    {
        $this->load->view('template/header');
        $this->load->view('books/index', [
            'books' => $this->books_model->get_books(),
        ]);
        $this->load->view('template/footer');
    }

    public function create()
    {
        if ($this->validate_form()) {
            $this->books_model->create_book($this->get_data());

            redirect('/books/index');
        } else {
            $this->load->view('template/header');
            $this->load->view('books/form');
            $this->load->view('template/footer');
        }
    }

    public function edit($id)
    {
        if ($this->validate_form()) {
            $this->books_model->update_book($this->get_data());

            redirect('/books/index');
        } else {
            $this->load->view('template/header');
            $this->load->view('books/form', $this->books_model->get_book($id));
            $this->load->view('template/footer');
        }
    }

    public function view($id)
    {
        $this->load->view('template/header');
        $this->load->view('books/view', [
            'book' => $this->books_model->get_book($id),
        ]);
        $this->load->view('template/footer');
    }

    public function delete($id)
    {
        $this->books_model->delete_book($id);

        redirect('/books/index');
    }

    private function validate_form()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules([
            [
                'field' => 'title',
                'label' => 'Título',
                'rules' => 'required',
            ],
            [
                'field' => 'isbn',
                'label' => 'ISBN',
                'rules' => 'required|integer',
            ],
            [
                'field' => 'author',
                'label' => 'Autor',
                'rules' => 'required',
            ],
            [
                'field' => 'price',
                'label' => 'Precio',
                'rules' => 'required|decimal',
            ],
            [
                'field' => 'review',
                'label' => 'Reseña',
                'rules' => 'required',
            ],
        ]);

        return $this->form_validation->run();
    }

    private function get_data()
    {
        $this->load->helper('url');

        return array(
            'id' => $this->input->post('id'),
            'title' => $this->input->post('title'),
            'isbn' => $this->input->post('isbn'),
            'author' => $this->input->post('author'),
            'price' => $this->input->post('price'),
            'cover' => $this->input->post('cover'),
            'review' => $this->input->post('review')
        );
    }
}
