<?php

class Api extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('books_model');
    }

    public function books()
    {
        header('Content-type: application/json');
        echo json_encode($this->books_model->get_books());
    }

    public function book($id)
    {
        header('Content-type: application/json');
        echo json_encode($this->books_model->get_book($id));
    }
}
