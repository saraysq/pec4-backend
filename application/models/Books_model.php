<?php

class Books_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_books()
    {
        return $this->db->get('books')->result_array();
    }

    public function get_book($id)
    {
        return $this->db->get_where('books', ['id' => $id])->row_array();
    }

    public function create_book($data)
    {
        return $this->db->insert('books', $data);
    }

    public function update_book($data)
    {
        return $this->db->update('books', $data, ['id' => $data['id']]);
    }

    public function delete_book($id)
    {
        return $this->db->delete('books', ['id' => $id]);
    }
}
