<div class="row">
    <div class="col-xs-12">
        <h1>
            <span class="glyphicon glyphicon-book" aria-hidden="true"></span>
            Listado de libros
        </h1>

        <div class="text-right">
            <a href="<?php echo site_url('books/create') ?>" class="btn btn-success">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                Crear libro
            </a>
        </div>

        <hr>

        <div class="table-responsive">
            <table class="table table-hover table-striped books-list">
                <thead>
                    <tr>
                        <th></th>
                        <th>Título</th>
                        <th>ISBN</th>
                        <th>Autor</th>
                        <th>Precio</th>
                    </tr>
                </thead>

                <tbody>
                    <?php if (count($books)): ?>
                        <?php foreach ($books as $book): ?>
                            <tr>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <span class="caret"></span>
                                        </button>

                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="<?php echo site_url('books/view/' . $book['id']) ?>">
                                                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                                    Ver
                                                </a>
                                            </li>

                                            <li>
                                                <a href="<?php echo site_url('books/edit/' . $book['id']) ?>">
                                                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                                    Editar
                                                </a>
                                            </li>

                                            <li class="divider"></li>

                                            <li>
                                                <a href="<?php echo site_url('books/delete/' . $book['id']) ?>">
                                                    <span class="glyphicon glyphicon-erase" aria-hidden="true"></span>
                                                    Borrar
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="book-title"><?php echo $book['title'] ?></td>
                                <td><?php echo $book['isbn'] ?></td>
                                <td><?php echo $book['author'] ?></td>
                                <td><?php echo $book['price'] ?> €</td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
