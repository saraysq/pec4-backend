<div class="row">
    <div class="col-xs-12">
        <h1>
            <span class="glyphicon glyphicon-book" aria-hidden="true"></span>
            Ficha de libro
        </h1>

        <div class="text-right">
            <div class="btn-group">
                <a href="<?php echo site_url('books') ?>" class="btn btn-default">
                    <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                    Volver al listado
                </a>

                <a href="<?php echo site_url('books/edit/' . $book['id']) ?>" class="btn btn-info">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    Editar libro
                </a>
            </div>
        </div>

        <hr>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h2><?php echo $book['title'] ?></h2>
    </div>

    <div class="col-sm-8 col-sm-push-4">
        <dl>
            <dt>ISBN</dt>
            <dd><?php echo $book['isbn'] ?></dd>

            <dt>Autor</dt>
            <dd><?php echo $book['author'] ?></dd>

            <dt>Precio</dt>
            <dd><?php echo $book['price'] ?> €</dd>

            <dt>Reseña</dt>
            <dd><?php echo nl2br($book['review']) ?></dd>
        </dl>
    </div>

    <div class="col-sm-4 col-sm-pull-8">
        <img src="<?php echo $book['cover'] ?>" class="img-responsive">
    </div>
</div>
