<div class="row">
    <div class="col-xs-12">
        <h1>
            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
            <?php if (isset($id) && $id > 0): ?>
                Editar libro
            <?php else: ?>
                Crear libro
            <?php endif ?>
        </h1>

        <div class="text-right">
            <div class="btn-group">
                <a href="<?php echo site_url('books') ?>" class="btn btn-default">
                    <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
                    Volver al listado
                </a>

                <?php if (isset($id) && $id > 0): ?>
                    <a href="<?php echo site_url('books/view/' . $id) ?>" class="btn btn-info">
                        <span class="glyphicon glyphicon-book" aria-hidden="true"></span>
                        Ver ficha
                    </a>
                <?php endif ?>
            </div>
        </div>

        <hr>

        <?php echo validation_errors() ?>

        <?php echo form_open(isset($id) && $id > 0 ? 'books/edit/' . $id : 'books/create', ['class' => 'form-horizontal']) ?>
            <input type="hidden" name="id" value="<?php echo set_value('id', isset($id) ? $id : '') ?>">

            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Título</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="title" value="<?php echo set_value('title', isset($title) ? $title : '') ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="isbn" class="col-sm-2 control-label">ISBN</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="isbn" value="<?php echo set_value('isbn', isset($isbn) ? $isbn : '') ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="author" class="col-sm-2 control-label">Autor</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="author" value="<?php echo set_value('author', isset($author) ? $author : '') ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="price" class="col-sm-2 control-label">Precio</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="price" value="<?php echo set_value('price', isset($price) ? $price : '') ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="cover" class="col-sm-2 control-label">Portada</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="cover" value="<?php echo set_value('cover', isset($cover) ? $cover : '') ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="review" class="col-sm-2 control-label">Reseña</label>
                <div class="col-sm-10">
                    <textarea name="review" class="form-control" rows="8"><?php echo set_value('review', isset($review) ? $review : '') ?></textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success">
                        <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
                        Guardar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
