<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_books_table extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'isbn' => array(
                'type' => 'INT',
                'constraint' => 13,
                'null' => false,
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => false,
            ),
            'price' => array(
                'type' => 'decimal',
                'constraint' => '5,2',
                'null' => false,
            ),
            'review' => array(
                'type' => 'TEXT',
                'null' => false,
            ),
            'author' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => false,
            ),
            'cover' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => false,
            ),
        ));

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('books');
    }

    public function down()
    {
        $this->dbforge->drop_table('books');
    }
}
